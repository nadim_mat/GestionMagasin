/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tuniprob.gestionmagasin.entity;

/**
 *
 * @author Nadim
 */
public class Caissier extends Employe{
    private int numeroDeCaisse;

    public Caissier(int id, String nom, String adresse, int nbrHeures,int numerDeCaisse) {
        super(id, nom, adresse, nbrHeures);
        this.numeroDeCaisse = numerDeCaisse;
    }

    public int getNumeroDeCaisse() {
        return numeroDeCaisse;
    }

    public void setNumeroDeCaisse(int numeroDeCaisse) {
        this.numeroDeCaisse = numeroDeCaisse;
    }

    @Override
    public String toString() {
        return super.toString()+" Numéro de caisse : "+numeroDeCaisse;
    }

    public float calculerSalaire(){
        if (nbrHeures>180){
            return (float) ((nbrHeures *5) +  (nbrHeures-180)*5.75);
            
        }
        else return nbrHeures*5;
    }
    
    

    
    
    
    
}
