/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tuniprob.gestionmagasin.entity;

import java.util.HashSet;
import java.util.Set;
import util.MagasinPleinException;

/**
 *
 * @author Nadim
 */
public class Magasin {

    private int id;
    public static final int CAPACITE = 50;
    private String adresse,nom;
    private Set<Produit> produits = new HashSet<>();
    private int NombreProduits;
    public static final int MAX_EMPLOYEES = 20;
    
    private Set<Employe> employees = new HashSet<>();

    public boolean ajouterProduit(Produit p) throws MagasinPleinException {
        if (NombreProduits >= CAPACITE) {
            throw new MagasinPleinException("Magasin plein");
            

        }
        else return produits.add(p);
    }

    public Magasin(int id,String nom ,String adresse) {
        this.id = id;
        this.adresse = adresse;
        this.nom = nom;
    }

    @Override
    public String toString() {

        String products = "Produits : \n";

        for (Produit p : produits) {

            products += "\nNom : " + p.getLib() + " Prix : " + p.getPrix();

        }

        return "Magasin{\n" + "id=" + id + ",Nom = "+nom+" adresse=" + adresse +
                ", NombreProduits=" + NombreProduits + "\n" + products + "\n}"
                +"Emplyées : "+employees;
    }

    public int getNombreProduits() {
        return NombreProduits;
    }

    public boolean chercherProduit(Produit p) {

        return produits.contains(p);
    }
    
    
    public boolean supprimerProduit(Produit p){
//        int count = NombreProduits;
//         for (int i = 0; i < count; i++) {
//             if(p.comparer(produits[i])){
//              for (int j = i; j < count-1; j++) {
//                     produits[j] = produits[j+1];
//                 }
//                 count--;
//                 
//                 return true;
//             }
//                 
//         }
//            return false;
    return produits.remove(p);
     
     }
    
    public static Magasin maxProduits(Magasin m1,Magasin m2){
        
        if (m1.getNombreProduits()>m2.getNombreProduits()){
            return m1;
        }
        else if (m1.getNombreProduits()<m2.getNombreProduits())
            return m2;
        else return m1;
    }
    
    public boolean ajouterEmploye(Employe e){
        
        if (employees.size()<MAX_EMPLOYEES){
            return employees.add(e);
        }
        else{
            System.err.println("Max employee");
            return false;
        }
    }
    
    
     public float calculStock(){
         float tot = 0;
        for (Produit p:produits){
            if (p instanceof ProduitFruit){
                ProduitFruit pf = (ProduitFruit)p;
                tot+=(float)pf.getPrix();
            }
            
        }
        return tot;
    }

}
