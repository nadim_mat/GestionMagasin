/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tuniprob.gestionmagasin.entity;

import java.util.Date;

/**
 *
 * @author Nadim
 */
public class Produit {

    protected int id;
    private String  marque;
    protected String lib;
    private float prix;
    private Date dateExp;

    public void afficher() {
        System.out.println("Produit{" + "id=" + id + ", lib=" + lib + ", marque=" + marque + ", prix=" + prix + '}');
    }

    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", lib=" + lib + ", marque=" + marque + ", prix=" + prix + ", dateExp=" + dateExp + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLib() {
        return lib;
    }

    public void setLib(String lib) {
        this.lib = lib;
    }

    public String getMarque() {
        return marque;
    }

    

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        if (prix < 0) {
            System.err.println("Le prix ne doit pas être négatif");
        } else {
            this.prix = prix;
        }
    }

    public Date getDateExp() {
        return dateExp;
    }

    public void setDateExp(Date dateExp) {
        this.dateExp = dateExp;
    }

    public Produit() {
    }

    public Produit(int id, String lib, String marque, float prix, Date dateExp) {
        if (prix < 0) {
            System.err.println("Le prix ne doit pas être négatif");
        } else {
            this.id = id;
            this.lib = lib;
            this.marque = marque;
            this.prix = prix;
            this.dateExp = dateExp;
        }
    }

    public Produit(int id, String lib, String marque) {
        this.id = id;
        this.lib = lib;
        this.marque = marque;

    }
    
    public boolean comparer(Produit p){
        return this.id == p.getId() && this.lib.equals(lib) && this.prix == p.getPrix();
    }
    
    public static boolean comparer(Produit p1,Produit p2){
        return p1.getId() == p2.getId() && p1.getLib().equals(p2.getLib()) && p1.getPrix() == p2.getPrix();
    }

    @Override
    public boolean equals(Object obj) {
       
        if (obj != null ){
            
            if (obj instanceof Produit){
                Produit p = (Produit) obj;
                return this.id == p.getId() && this.lib.equals(lib) && this.prix == p.getPrix();
            }else return false;
            
        }else return false;
    }
    
    
   
    
    

}
