/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tuniprob.gestionmagasin.entity;

import java.util.Date;

/**
 *
 * @author Nadim
 */
public class ProduitFruit extends Produit implements Critere{
    
    private float quantite;
    private Saison saison;

    public ProduitFruit( int quantite, Saison saison, int id, String lib, String marque, float prix, Date dateExp) {
        super(id, lib, marque, prix, dateExp);
       
        this.quantite = quantite;
        this.saison = saison;
    }

    

    

    public float getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Saison getSaison() {
        return saison;
    }

    public void setSaison(Saison saison) {
        this.saison = saison;
    }
    
    public ProduitFruit(int id,String lib,float quantite,Saison saison){
        this.id = id;
        this.lib = lib;
        this.quantite = quantite;
        this.saison = saison;
    }

    @Override
    public boolean estFrais(String saison) {
        return saison.equals(this.saison.toString());    
    }
    
    
}
