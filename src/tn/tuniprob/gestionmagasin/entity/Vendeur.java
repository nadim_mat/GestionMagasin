/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tuniprob.gestionmagasin.entity;

/**
 *
 * @author Nadim
 */
public class Vendeur extends Employe{
    
    private int tauxDeVente;

    public Vendeur(int tauxDeVente, int id, String nom, String adresse, int nbrHeures) {
        super(id, nom, adresse, nbrHeures);
        this.tauxDeVente = tauxDeVente;
    }

    public int getTauxDeVente() {
        return tauxDeVente;
    }

    public void setTauxDeVente(int tauxDeVente) {
        this.tauxDeVente = tauxDeVente;
    }

    @Override
    public String toString() {
        return super.toString()+" Taux vente : "+tauxDeVente; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public float calculerSalaire(){
        return 450 + (450*tauxDeVente)/100;
    }
    
}
