/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.tunuprob.gestionmagasin.main;

import tn.tuniprob.gestionmagasin.entity.Magasin;
import tn.tuniprob.gestionmagasin.entity.ProduitFruit;
import tn.tuniprob.gestionmagasin.entity.ProduitLegume;
import tn.tuniprob.gestionmagasin.entity.Saison;
import util.MagasinPleinException;

/**
 *
 * @author Nadim
 */
public class GestionMagasin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Produit p = new Produit();
//        Produit p1 = new Produit(1021, "Lait", "Délice"),
//                p2 = new Produit(2510, "Yaoutrt", "Vitalait"),
//                p3 = new Produit(3250, "Tomate", "Sicam");
//
////        
////        
////        
////        
////        p1.afficher();
////        p2.afficher();
////        p3.afficher();
////        
////        p1.setPrix(.7f);
////        p2.setPrix(.32f);
////       
////        
////        System.out.println("-----------------------");
////        p1.afficher();
////        p2.afficher();
////       
////        
////        System.out.println("-------------------");
////        System.out.println(p1);
////        System.out.println(p2);
////        System.out.println(p3);
////        
////        p1.setDateExp(new Date(2016, 10, 10));
////        p2.setDateExp(new Date(2016, 10, 10));
////        p3.setDateExp(new Date(2016, 10, 10)); 
////        
////        System.out.println("----------------------------");
////        
////        System.out.println(p1);
////        System.out.println(p2);
////        System.out.println(p3);
////        
////        
////        
////        Magasin m1 = new Magasin(0, "b");
////        m1.ajouterProduit(p1);
////        System.out.println(m1);
//        System.out.println("------------------------------");
//
//        Magasin m1 = new Magasin(1, "Carrefour", "centre ville");
//        Magasin m2 = new Magasin(2, "Monoprix", "menzah 6");
//
//        Caissier c11 = new Caissier(1, "Nadim", "blabla", 10, 2);
//        Caissier c12 = new Caissier(2, "Salah", "blabla", 20, 3);
//        Vendeur v11 = new Vendeur(3, 3, "V", "blabla", 20);
//        Responsable r11 = new Responsable(20, 4, "Re", "Tunis", 50);
//
//        Caissier c21 = new Caissier(5, "", "", 20, 5);
//        Vendeur v21 = new Vendeur(1, 5, "V", "blabla", 20);
//        Vendeur v22 = new Vendeur(2, 6, "V", "blabla", 20);
//        Vendeur v23 = new Vendeur(8, 7, "V", "blabla", 20);
//        Responsable r21 = new Responsable(20, 8, "Re", "Tunis", 50);
//
//        m1.ajouterEmploye(c11);
//        m1.ajouterEmploye(c12);
//        m1.ajouterEmploye(v11);
//        m1.ajouterEmploye(r11);
//
//        m2.ajouterEmploye(c21);
//        m2.ajouterEmploye(v21);
//        m2.ajouterEmploye(v22);
//        m2.ajouterEmploye(c21);
//        m2.ajouterEmploye(c21);
//        
//        System.out.println(m1);
//        System.out.println(m2);
//        
//        System.out.println(v21.calculerSalaire());

        ProduitFruit p1 = new ProduitFruit(1254, "Fraise", 12.3f, Saison.Mars);
        ProduitFruit p2 = new ProduitFruit(1224, "Pastèque", 50f, Saison.Juin);
        ProduitFruit p3 = new ProduitFruit(7896, "Fruit", 25.6f, Saison.Décembre);
        ProduitLegume p4 = new ProduitLegume(5821, "Artichauts", 14, Saison.Janvier);

        Magasin m = new Magasin(1, "Carrefour", "La marsa");
        try {
            m.ajouterProduit(p1);
            m.ajouterProduit(p2);
            m.ajouterProduit(p3);
            m.ajouterProduit(p4);
        } catch (MagasinPleinException e) {
            e.printStackTrace();
        }

        p4.estFrais("Mars");

    }

}
